import datetime

from peewee import *

# SQLite database using WAL journal mode and 64MB cache.
sqlite_db = SqliteDatabase('app.db', pragmas={'journal_mode': 'wal'})


class BaseDbModel(Model):
    """A base model that will use our Sqlite database."""

    class Meta:
        database = sqlite_db


class GameDB(BaseDbModel):
    name = TextField()


class PlayerDB(BaseDbModel):
    game_id = ForeignKeyField(model=GameDB, field=GameDB.id)
    timestamp = DateTimeField(default=datetime.datetime.now())
    score = FloatField()


class AnswersDb(BaseDbModel):
    game_id = ForeignKeyField(model=GameDB, field=GameDB.id)
    question = TextField()
    answer = TextField()
    timestamp = DateTimeField(default=datetime.datetime.now())


if __name__ == '__main__':
    with sqlite_db:
        sqlite_db.create_tables([GameDB, PlayerDB, AnswersDb])
