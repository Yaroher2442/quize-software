class NoLeadError(BaseException):
    def __init__(self):
        pass

    def __str__(self):
        return "No lead found"
