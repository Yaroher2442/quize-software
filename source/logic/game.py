from __future__ import annotations

from typing import Tuple

from source.logic.scenario import *
from sanic.server.websockets.impl import WebsocketImplProtocol
from source.logic.events import *
import json
from source.logic.errors import NoLeadError
from loguru import logger


class States:
    register = "register"
    waiting_tactics = "waiting_tactics"
    tactic_ok = "tactic_ok"
    waiting_answers = "waiting_answers"
    answers_ok = "answers_ok"
    image_question = "image_question"
    video_before = "video_before"
    video_after = "video_after"


class TacticsNames:
    regular = "regular"
    one_for_all = "one_for_all"
    question_bet = "question_bet"
    all_in = "all_in"
    team_bet = "team_bet"


class GameGlobalState:
    scenario = GameConf(**json.load(open("config/scenario.json"))["game"])
    state: str
    current_round: int = 0
    current_question: int = 0

    def get_round(self) -> RoundConf:
        return self.scenario.rounds[self.current_round]

    def get_question(self) -> QuestionConf:
        return self.scenario.rounds[self.current_round].questions[self.current_question]

    def get_next_question(self) -> QuestionConf:
        if self.current_question + 1 > len(self.get_round().questions):
            self.current_question = 0
            self.current_round += 1
        else:
            self.current_question += 1
        return self.get_question()

    def get_round_tactics(self) -> dict:
        round_tactics = vars(self.get_round().settings.tactics)
        if not all(round_tactics.values()):
            logger.debug("Chose ROUND-set tactics")
            return round_tactics
        else:
            logger.debug("Chose GAME-set tactic")
            return vars(self.scenario.settings.tactics)

    def get_video(self, before: bool = False, after: bool = False) -> dict:
        if before:
            return {"video": self.get_question().media_data.video_before}
        if after:
            return {"video": self.get_question().media_data.video_after}


class Team:
    client_ip: str = ""
    name: str = ""
    score: float = 0
    chosen_tactic: str = ""
    chosen_team_bet: Tuple[Team, int]
    chosen_score_bet: int = 0
    current_answer: Union[List[int], int, str] = ""
    tactic_balance: Dict[str, int]

    def __init__(self, ws: WebsocketImplProtocol):
        self.ws = ws
        self.client_ip = self.ws.io_proto.conn_info.client_ip

    async def send_data(self, data: dict):
        await self.ws.send(json.dumps(data))

    def remove_temp_data(self):
        self.chosen_tactic = ""
        self.current_answer = ""
        self.chosen_score_bet = None
        self.chosen_team_bet = None

    def get_name(self):
        return self.name


class Lead:
    teams: Dict[str, Team]
    monitors: List[Monitor]
    game_state: GameGlobalState

    def __init__(self):
        self.ws = None
        self.teams = {}
        self.monitors = []
        self.game_state = GameGlobalState()

    def attach_to_socket(self, ws: WebsocketImplProtocol):
        self.ws = ws

    def add_or_restore_team(self, ip: str, team: Team):
        if not self.ws:
            raise NoLeadError
        if ip not in self.teams.keys():
            team.tactic_balance = self.game_state.get_round_tactics()
            self.teams.update({ip: team})
        else:
            for k, v in vars(self.teams[ip]).items():
                setattr(team, k, v)
            self.teams[ip] = team

    def add_monitor(self, monitor: Monitor):
        if not self.ws:
            raise NoLeadError
        self.monitors.append(monitor)

    async def sent_data(self, data: dict, to_team: bool = True, to_monitors: bool = True):
        if to_team:
            for team in self.teams.values():
                await team.send_data(data)
        if to_monitors:
            for monitor in self.monitors:
                await monitor.send_data(data)

    async def accept_event(self, connection_ip: str, event: Event):
        logger.debug(event)
        if not self.ws:
            raise NoLeadError
        if isinstance(event, LeadGetState):
            await self.ws.send(json.dumps({"state": vars(self.game_state)}))
        elif isinstance(event, StartGame):
            dt = {"type": "start_game"}
            dt.update({"data": self.game_state.get_question().dict()})
            await self.sent_data(dt, to_team=True, to_monitors=True)
        elif isinstance(event, NextQuestion):
            for team in self.teams.values():
                team.remove_temp_data()
            dt = {"type": "next_question"}
            dt.update({"data": self.game_state.get_next_question().dict()})
            await self.sent_data(dt)
        elif isinstance(event, VideoBefore):
            await self.sent_data(self.game_state.get_video(before=True), to_team=False)
        elif isinstance(event, VideoAfter):
            await self.sent_data(self.game_state.get_video(after=True), to_team=False)
        if "Team" in event.__class__.__name__:
            curr_team = self.teams[connection_ip]
            if isinstance(event, TeamSetName):
                curr_team.name = event.name
                await self.sent_data({"type": "new_team", "data": {"team_name": event.name}})
            elif isinstance(event, TeamChoseTacticRegular) \
                    or isinstance(event, TeamChoseTacticOneForAll) \
                    or isinstance(event, TeamChoseTacticAllIn):
                curr_team.chosen_tactic = event.tactic_name
            elif isinstance(event, TeamChoseTacticTeamBet):
                curr_team.chosen_tactic = event.tactic_name
                cell_team = [team for team in self.teams.values() if team.name == event.team_name][0]
                curr_team.chosen_team_bet = (cell_team, event.bet)
                curr_team.tactic_balance[curr_team.chosen_tactic] -= 1
            elif isinstance(event, TeamChoseTacticQuestionBet):
                curr_team.chosen_tactic = event.tactic_name
                curr_team.chosen_score_bet = event.score_bet
            elif isinstance(event, TeamAnswered):
                curr_team.current_answer = event.answer
            team_state_str = "\n".join([f"{k}:{v}" for k, v in vars(curr_team).items() if not "ws" in k])
            logger.debug(f"Состояние команды {curr_team.name}: \n{team_state_str}")


class Monitor:
    def __init__(self, ws: WebsocketImplProtocol):
        self.ws = ws

    async def send_data(self, data: dict):
        await self.ws.send(json.dumps(data))
