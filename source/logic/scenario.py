from pydantic import BaseModel, ValidationError, validator, constr
from typing import List, Dict, Optional, Union


class AllowConf(BaseModel):
    select: bool
    multiselect: bool
    text: bool


class TacticsConf(BaseModel):
    remove_answer: int
    one_for_all: int
    question_bet: int
    all_in: int
    team_bet: int


class SettingsConf(BaseModel):
    allow_types: AllowConf
    tactics: TacticsConf


class ImageConf(BaseModel):
    displayed: bool
    path: str


class MediaDataConf(BaseModel):
    image: ImageConf
    video_before: str
    video_after: str


class QuestionConf(BaseModel):
    type: str
    question: str
    answers: Union[str, List[str]]
    correct_answer: Union[str, List[int], int]
    time_to_answer: int
    media_data: MediaDataConf


class BlitzQuestionConf(BaseModel):
    type: str
    question: str
    correct_answer: str


class BlitzConf(BaseModel):
    type: str
    time_to_answer: int
    blitz_questions: List[BlitzQuestionConf]


class RoundConf(BaseModel):
    is_test: bool
    name: str
    settings: SettingsConf
    questions: List[Union[QuestionConf, BlitzConf]]
    time_to_answer: int


class GameConf(BaseModel):
    name: str
    settings: SettingsConf
    rounds: List[RoundConf]




