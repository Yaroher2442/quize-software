from typing import Union, List

from pydantic import BaseModel, constr, ValidationError


class Event(BaseModel):
    event: str

    def __str__(self):
        return f"EVENT: Class: {self.__class__.__name__} ; {vars(self)}"


class StartGame(Event):
    event: constr(regex=r'setup_game')


class VideoBefore(Event):
    event: constr(regex=r'video_before')


class VideoAfter(Event):
    event: constr(regex=r'video_after')


class NextQuestion(Event):
    event: constr(regex=r'next_question')


class LeadGetState(Event):
    event: constr(regex=r'lead_get_state')


class TeamSetName(Event):
    event: constr(regex=r'set_team_name')
    name: str


class TeamChoseTactic(Event):
    event: constr(regex=r'team_chose_tactic')
    tactic_name: str


class TeamChoseTacticRegular(Event):
    event: constr(regex=r'team_chose_tactic_regular')
    tactic_name: str


class TeamChoseTacticOneForAll(Event):
    event: constr(regex=r'team_chose_tactic_one_for_all')
    tactic_name: str


class TeamChoseTacticAllIn(Event):
    event: constr(regex=r'team_chose_tactic_all_in')
    tactic_name: str


class TeamChoseTacticQuestionBet(Event):
    event: constr(regex=r'team_chose_tactic_question_bet')
    tactic_name: str
    score_bet: Union[str, int]


class TeamChoseTacticTeamBet(Event):
    event: constr(regex=r'team_chose_tactic_team_bet')
    tactic_name: str
    team_name: str
    bet: Union[str, int]


class TeamAnswered(Event):
    event: constr(regex=r'team_answered')
    answer: Union[List[int], str, int]


def get_event_from_json(json_data: dict) -> Event:
    returned_event = None
    validate_err = None
    for event in Event.__subclasses__():
        try:
            returned_event = event(**json_data)
        except ValidationError as err:
            validate_err = err
            continue
    if not returned_event:
        raise validate_err
    return returned_event
