import json
from json import JSONDecodeError
from pydantic import ValidationError
from loguru import logger
from sanic import Sanic, response
from sanic.server.websockets.impl import WebsocketImplProtocol
from websockets.connection import State

from source.logic.game import Lead, Team, Monitor
from source.logic.events import get_event_from_json
from sanic.request import Request
from source.logic.errors import NoLeadError


class ServerApp:
    def __init__(self):
        self.app = Sanic(__name__)
        self.game_lead = Lead()
        self.setup_routes()
        self.register_api()

    def register_api(self):
        self.app.static("/player", "source/server/templates/player/index.html")
        self.app.static("/lead", "source/server/templates/lead/index.html")
        self.app.static("/monitor", "source/server/templates/monitor/index.html")

    def serve(self):
        self.app.run("0.0.0.0")

    def setup_routes(self):
        @self.app.route("/")
        async def home(request):
            return response.text("Hello Sanic")

        @self.app.websocket("ws/<path:str>")
        async def player_socket(request, ws: WebsocketImplProtocol, path):
            connection_ip = ws.io_proto.conn_info.client_ip
            logger.debug(f"Open websocket on {connection_ip} with path: {path}")
            try:
                if path == "lead":
                    self.game_lead.attach_to_socket(ws)
                elif path == "team":
                    self.game_lead.add_or_restore_team(connection_ip, Team(ws))
                elif path == "monitor":
                    self.game_lead.add_monitor(Monitor(ws))
                else:
                    await ws.close(code=1014)
                while True:
                    income_data = await ws.recv()
                    try:
                        await self.game_lead.accept_event(connection_ip,get_event_from_json(json.loads(income_data)))
                    except ValidationError:
                        logger.error("Validate Event FAILED")
                        await ws.send("Validate Event FAILED")
                    except JSONDecodeError:
                        logger.error("Cant process non-json string")
                        await ws.send("Cant process non-json string")
            except NoLeadError:
                logger.debug(f"Can't find lead on connection {path}")
                await ws.send("Can't find lead, wait for connection")
