import os

# from config.config_ import Configurator
from source.server.app import ServerApp

if __name__ == '__main__':
    server = ServerApp()
    server.serve()

    # config = Configurator("config.json")
    # app.run(host="0.0.0.0")
