import json
import os
from loguru import logger


class Configurator:

    def __init__(self, path):
        self.config_path = path
        logger.debug("Load config...")
        if os.path.exists(self.config_path):
            with open(self.config_path, "r") as conf_file:
                self.main_conf = json.load(conf_file)
        else:
            exit("Config not found, add in folder config")
        self.player_conf = PlayerConf(self.main_conf["entries"]["player_conf"])
        self.lead_conf = LeadConf(self.main_conf["entries"]["lead_conf"])
        self.monitor_conf = MonitorConf(self.main_conf["entries"]["monitor_conf"])
        logger.debug("Config Already loads")


class PlayerConf:
    def __init__(self, conf):
        self.fl_conf: str
        self.fl_port: int
        self.socket_host: str
        self.socket_port: int
        for k, v in conf.items():
            setattr(self, k, v)


class LeadConf:
    def __init__(self, conf):
        self.fl_conf: str
        self.fl_port: int
        self.socket_host: str
        self.socket_port: int
        for k, v in conf.items():
            setattr(self, k, v)


class MonitorConf:
    def __init__(self, conf):
        self.fl_conf: str
        self.fl_port: int
        self.socket_host: str
        self.socket_port: int
        for k, v in conf.items():
            setattr(self, k, v)
